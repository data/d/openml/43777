# OpenML dataset: International-football-results

https://www.openml.org/d/43777

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Well, what happened was that I was looking for a semi-definite easy-to-read list of international football matches and couldn't find anything decent. So I took it upon myself to collect it for my own use. I might as well share it.
Content
This dataset includes 41,586 results of international football matches starting from the very first official match in 1972 up to 2019. The matches range from FIFA World Cup to FIFI Wild Cup to regular friendly matches. The matches are strictly men's full internationals and the data does not include Olympic Games or matches where at least one of the teams was the nation's B-team, U-23 or a league select team.
results.csv includes the following columns:
date - date of the match
hometeam - the name of the home team
awayteam - the name of the away team
homescore - full-time home team score including extra time, not including penalty-shootouts
awayscore - full-time away team score including extra time, not including penalty-shootouts
tournament - the name of the tournament
city - the name of the city/town/administrative unit where the match was played
country - the name of the country where the match was played
neutral - TRUE/FALSE column indicating whether the match was played at a neutral venue
Note on team and country names:
For home and away teams the current name of the team has been used. For example, when in 1882 a team who called themselves Ireland played against England, in this dataset, it is called Northern Ireland because the current team of Northern Ireland is the successor of the 1882 Ireland team. This is done so it is easier to track the history and statistics of teams.
For country names, the name of the country at the time of the match is used. So when Ghana played in Accra, Gold Coast in the 1950s, even though the names of the home team and the country don't match, it was a home match for Ghana. This is indicated by the neutral column, which says FALSE for those matches, meaning it was not at a neutral venue.
Acknowledgements
The data is gathered from several sources including but not limited to Wikipedia, fifa.com, rsssf.com and individual football associations' websites.
Inspiration
Some directions to take when exploring the data:
Who is the best team of all time
Which teams dominated different eras of football
What trends have there been in international football throughout the ages - home advantage, total goals scored, distribution of teams' strength etc
Can we say anything about geopolitics from football fixtures - how has the number of countries changed, which teams like to play each other
Which countries host the most matches where they themselves are not participating in
How much, if at all, does hosting a major tournament help a country's chances in the tournament
Which teams are the most active in playing friendlies and friendly tournaments - does it help or hurt them
The world's your oyster, my friend.
Contribute
If you notice a mistake or the results are being updated fast enough for your liking, you can fix that by submitting a pull request.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43777) of an [OpenML dataset](https://www.openml.org/d/43777). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43777/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43777/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43777/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

